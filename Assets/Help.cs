﻿using UnityEngine;
using System.Collections;

public class Help : MonoBehaviour {
	public GameObject one;
	public GameObject two;
	public GameObject three;
	private bool show = true;
	// Use this for initialization
	void Start () {
		Show ();
	}
	public void Show()
	{
		show = !show;
		Show (show);
	}
	private void Show(bool show)
	{
		one.GetComponent<UnityEngine.UI.Text> ().enabled = show;
		two.GetComponent<UnityEngine.UI.Text> ().enabled = show;
		three.GetComponent<UnityEngine.UI.Text> ().enabled = show;
	}
	// Update is called once per frame
	void Update () {
	
	}
}
