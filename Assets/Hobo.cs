﻿using UnityEngine;
using System.Collections;

public class Hobo : MonoBehaviour {
	void Awake()
	{
		}
	// Use this for initialization
	void Start () {
		foreach (var joint in GetComponentsInChildren<CharacterJoint>()) {
			joint.enableCollision = true;		
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
