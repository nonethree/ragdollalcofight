﻿using UnityEngine;
using System.Collections;

public class CatSpawner : MonoBehaviour {
	public GameObject cat_prefab;
	public float period;
	private float p;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		p -= Time.deltaTime;
		if (p < 0) {
			Instantiate(cat_prefab, transform.position, Random.rotation);
			p = period;
		}
	}
}
