﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {
	public GameObject PlayerOneController;
	public GameObject PlayerTwoController;
	public GameObject Bot;
	public MenuController menu_controller;
	public Canvas menu;
	public Canvas ui ;
	public Camera gameCamera;
	// Use this for initialization
	void Start () {
		DisableAll ();
	}

	public void EndGame() {
		GetComponent<ScreenFader> ().fadeState = ScreenFader.FadeState.In;
		StartCoroutine (RestartWithDelay ());
	}

	IEnumerator RestartWithDelay()
	{
		yield return new WaitForSeconds (2f);
		Application.LoadLevel("main 1");
	}

	// Update is called once per frame
	void Update () {
		if(null != menu_controller)
		if (menu_controller.onePlayer) {
			if(menu_controller.playerOneReady)
				OnePlayerGame();
		}
		else if(menu_controller.playerOneReady && menu_controller.playerTwoReady )
		{
			TwoPlayerGame();
		}
		if(Input.GetButton("Restart"))
		{
			Application.LoadLevel("main 1");
		}
		if (Input.GetButton ("Exit")) {
			Application.Quit();
		}
	}
	void DisableAll()
	{
		PlayerOneController.GetComponent<PlayerController> ().enabled = false;
		PlayerTwoController.GetComponent<Player2Controller> ().enabled = false;
		Bot.GetComponent<BotController> ().enabled = false;
		ui.gameObject.SetActive (false);
	}
	void OnePlayerGame()
	{
		PlayerOneController.GetComponent<PlayerController> ().enabled = true;
		Bot.GetComponent<BotController> ().enabled =  true;
		ui.gameObject.SetActive (true);
		menu.gameObject.SetActive (false);
	}
	void TwoPlayerGame()
	{
		PlayerOneController.GetComponent<PlayerController> ().enabled =  true;
		PlayerTwoController.GetComponent<Player2Controller> ().enabled =  true;
		ui.gameObject.SetActive (true);
		menu.gameObject.SetActive (false);
	}
}
