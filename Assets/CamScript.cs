﻿using UnityEngine;
using System.Collections;

public class CamScript : MonoBehaviour {

	public Transform fighter1;
	public Transform fighter2;

	public float zMax;
	public float fovMax;
	public float fovMin;

	void Update()
	{
		float t = Mathf.Abs (fighter1.transform.position.z - fighter2.transform.position.z) / zMax;
		GetComponent<Camera>().fieldOfView = Mathf.Lerp(fovMin, fovMax, t);
		Vector3 v = GetComponent<Transform> ().position;
		v.z = (fighter1.transform.position.z + fighter2.transform.position.z) / 2;
		GetComponent<Transform> ().position = v;

	}
}
