﻿using UnityEngine;
using System.Collections;

public class Player2Controller : MonoBehaviour {
	
	public DrunkController drunk;
	
	// Update is called once per frame
	void Update () {
		if (drunk != null) {
			float input = Input.GetAxis ("Vertical2");
			if (input != 0)
				drunk.MoveBody(input);
			
			input = Input.GetAxis ("Horizontal2");
			if (input != 0)
				drunk.MoveFeet(input);
			
			if (Input.GetButton("StrikeLeft2"))
			{
				drunk.StrikeLeft();
			}
			if (Input.GetButton("StrikeRight2"))
			{
				drunk.StrikeRight();
			}
		}
	}
}
