﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
public class MenuController : MonoBehaviour {
	public Transform MainMenu;
	public Transform PlayerOne;
	public Transform PlayerTwo;
	public GameObject[] currentFirstPlayerButtons = new GameObject[2];
	public GameObject[] mainMenuButtons = new GameObject[2];
	public GameObject[] firstPlayerButtons = new GameObject[2];
	public GameObject[] secondPlayerButtons = new GameObject[2];
	public float inputThreshold = 0.5f;
	public bool onePlayer = true;
	public bool playerOneReady = false;
	public bool playerTwoReady = false;
	private int sel_index1=0;
	private int sel_index2=0;
	private float prevInputTime1;
	private float prevInputTime2;
	// Use this for initialization
	void Start()
	{
		print (transform.name);
		HideAllMenu ();
		ShowMain ();

		prevInputTime1 = Time.time;
		prevInputTime2 = Time.time;


	}

	// Update is called once per frame
	void Update () {

		if (!playerOneReady) {
			if (Time.time - prevInputTime1 > inputThreshold) {

				if (Input.GetKey (KeyCode.W) ) 
					Select (currentFirstPlayerButtons,true, ref sel_index1);
				else if (Input.GetKey (KeyCode.S)) 
					Select (currentFirstPlayerButtons,false, ref sel_index1);
				prevInputTime1 = Time.time;
			
			

				if (Input.GetKey (KeyCode.Joystick1Button0) || Input.GetButton("StrikeRight")) {

					Submit (currentFirstPlayerButtons [sel_index1]);
				}
				if (Input.GetKey (KeyCode.Joystick1Button1)|| Input.GetButton("StrikeLeft")) {
				
					Cancel (currentFirstPlayerButtons [sel_index1]);
				}
			}
		}
		if (!playerTwoReady) {
			if (Time.time - prevInputTime2 < inputThreshold) {
				if (Input.GetKey (KeyCode.UpArrow)) 
					Select (secondPlayerButtons, true, ref sel_index2);
				else if (Input.GetKey (KeyCode.DownArrow)) 
					Select (secondPlayerButtons, false, ref sel_index2);
				prevInputTime2 = Time.time;
			
				if (Input.GetKey (KeyCode.Joystick2Button0) || Input.GetButton("StrikeRight2")) {
			
				Submit (secondPlayerButtons [sel_index2]);
			}
			}
		}

	}
	void Submit(GameObject selected_object)
	{
		ExecuteEvents.Execute<ISubmitHandler> (selected_object, new BaseEventData (EventSystem.current), ExecuteEvents.submitHandler);
	}
	void Cancel(GameObject selected_object)
	{
		ExecuteEvents.Execute<ICancelHandler> (selected_object, new BaseEventData (EventSystem.current), ExecuteEvents.cancelHandler);
	}
	void Select(GameObject[] select_array, bool next, ref int select_index)
	{
		if (next) {
			if (++select_index > select_array.Length - 1) {
				select_index = 0;
			}
		} else {
			if (--select_index < 0) {
				select_index = select_array.Length - 1;
			}
		}
		foreach (var go in select_array) {
			ExecuteEvents.Execute<IDeselectHandler>(go, new BaseEventData(EventSystem.current), ExecuteEvents.deselectHandler);
		}

		ExecuteEvents.Execute<ISelectHandler>(select_array[select_index], new BaseEventData(EventSystem.current), ExecuteEvents.selectHandler);
	}
	void HideAllMenu()
	{
		MainMenu.gameObject.SetActive (false);
		PlayerOne.gameObject.SetActive (false);
		PlayerTwo.gameObject.SetActive (false);
	}
	void ShowMenu(Transform menu)
	{
		menu.gameObject.SetActive (true);
	}
	public void ShowMain()
	{
		HideAllMenu ();
		ShowMenu (MainMenu);
		currentFirstPlayerButtons = mainMenuButtons;

	}
	public void ShowPlayerOne()
	{
		HideAllMenu ();
		ShowMenu (PlayerOne);
		currentFirstPlayerButtons = firstPlayerButtons;
		onePlayer = true;
	}
	public void ShowPlayerTwo()
	{
		HideAllMenu ();
		currentFirstPlayerButtons = firstPlayerButtons;
		ShowMenu (PlayerOne);
		ShowMenu (PlayerTwo);
		onePlayer = false;
		playerTwoReady = true;
		Select (secondPlayerButtons, true, ref sel_index1);
	}
	public void SetPlayerOneReady()
	{
		playerOneReady = true;

	}
	public void SetPlayerTwoReady()
	{
		playerTwoReady = true;
	}
	public void HandleSecondPlayerInput()
	{

	}
}
