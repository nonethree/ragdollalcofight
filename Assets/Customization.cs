﻿using UnityEngine;
using System.Collections;

public class Customization : MonoBehaviour {
	public Rigidbody RightElbow;
	public Rigidbody LeftElbow;
	public GameObject WeaponPlaceLeft;
	public GameObject WeaponPlaceRight;
	public GameObject Beard;
	public GameObject Hat;

	public Renderer Render;
	public GameObject[] WeaponsL;
	public GameObject[] WeaponsR;
	public GameObject[] Hats;
	public GameObject[] Beards;
	public Texture[] Textures;

	// Use this for initialization
	void Start () {
		Customize ();
	}
	
	// Update is called once per frame
	void Update () {

	}

	GameObject RandomObject(GameObject[] array)
	{
		return array [Random.Range (0, array.Length)];
	}
	Texture RandomTexture(Texture[] array)
	{
		return array [Random.Range (0, array.Length)];
	}
	public void Customize()
	{
		CustomizeWeapon(WeaponsR,WeaponPlaceRight, RightElbow);
		CustomizeWeapon (WeaponsL, WeaponPlaceLeft, LeftElbow);
		CustomizeHat ();
		CustomizeBeard ();
		CustomizeTexture ();
	}
	private void CustomizeHat()
	{
		GameObject new_hat = Instantiate(RandomObject (Hats), Hat.transform.position,Hat.transform.rotation ) as GameObject ;
		new_hat.transform.parent = Hat.transform.parent;
		Destroy (Hat);
		Hat = new_hat;
	}
	private void CustomizeTexture()
	{
		Texture tex = RandomTexture (Textures);
		Material mat = Render.material;
//		print (mat.name);
		mat.SetTexture(0, tex) ;
		Render.material = mat;
	}
	private void CustomizeBeard()
	{
		GameObject new_beard = Instantiate(RandomObject (Beards), Beard.transform.position,Beard.transform.rotation ) as GameObject ;
		new_beard.transform.parent = Hat.transform.parent;
		Destroy (Beard);
		Beard = new_beard;
	}
	private void CustomizeWeapon(GameObject[] weapon_array, GameObject WeaponPlace, Rigidbody connected_body)
	{	
		foreach (var wpn in WeaponPlace.GetComponentsInChildren<Transform> ()) {
		
			if(wpn.transform.parent.Equals(WeaponPlace.transform))
			   Destroy(wpn.gameObject);
		}


		GameObject weapon = Instantiate(RandomObject (weapon_array), WeaponPlace.transform.position,WeaponPlace.transform.rotation ) as GameObject ;

		weapon.transform.SetParent (WeaponPlace.transform);

		weapon.GetComponentInChildren<CharacterJoint> ().connectedBody = connected_body;

	}

}
