﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
public class CamPreset
{
	public Vector3 pivot;
	public Quaternion rotation;
	public bool isOrtographic;
	public CamPreset(Vector3 _pivot,Quaternion rot, bool isOrto)
	{
		pivot= _pivot;
		rotation = rot;
		isOrtographic= isOrto;
	}
}
public class SceneViewPresetsWindow:  EditorWindow
{
	[MenuItem("#BearUtils/Cam Utils")]
	public static void Init()
	{
		GetWindow<SceneViewPresetsWindow>().Show();
	}
	bool cameraUtils;
	bool save;
	bool load=true;
	string name;
	private Dictionary<string, CamPreset> camPresets = new Dictionary<string, CamPreset>();
	void Start()
	{
		name = (camPresets.Count+1).ToString();
	}
	void OnGUI()
	{
		if (GUILayout.Button("#Camera utils"))
		{
			cameraUtils = !cameraUtils;
			save = true;
		}
		if (cameraUtils)
		{
			GUILayout.BeginHorizontal();


			GUILayout.EndHorizontal();

		
		if (save) {
			

			GUILayout.BeginHorizontal();
			GUILayout.Label("Cam name");
			name = GUILayout.TextField(name, 10,  GUILayout.ExpandWidth(true));
			

			if (GUILayout.Button("Save") )
			{
				SaveCurrentCamera(name);
					load = true;
			}
			if (GUILayout.Button("Delete") )
			{
					DeleteCamera(name);
					//save = false;
			}
			GUILayout.EndHorizontal();

		}
		
		}
		if (load) {
			GUILayout.Label("LoadCam");
		//
			GUILayout.BeginVertical();
			var cam_enum = camPresets.GetEnumerator();
			while(cam_enum.MoveNext())
			{
				if(GUILayout.Button(cam_enum.Current.Key) )
				{
					LoadCamera(cam_enum.Current.Key);
					
				}
			}
			GUILayout.EndVertical();

		}
	}
	void SaveCurrentCamera(string name)
	{
		CamPreset preset = new CamPreset (SceneView.lastActiveSceneView.pivot,SceneView.lastActiveSceneView.rotation,SceneView.lastActiveSceneView.orthographic );
		if (camPresets.ContainsKey (name))
			camPresets [name] = preset;
		else
			camPresets.Add (name, preset);
	}
	void DeleteCamera(string name)
	{
		if (camPresets.ContainsKey (name))
			camPresets.Remove(name);
	}
	void LoadCamera(string name)
	{
		SceneView.lastActiveSceneView.pivot = camPresets [name].pivot;
		SceneView.lastActiveSceneView.rotation = camPresets [name].rotation;
		SceneView.lastActiveSceneView.orthographic = camPresets [name].isOrtographic;
		SceneView.lastActiveSceneView.Repaint ();
	}
}
