﻿using UnityEngine;
using System.Collections;

public class Wheel : MonoBehaviour {
	public float angle=20;
	public float speed;
	public float cur;
	private float startinputtime;
	private Transform starttransform;
	// Use this for initialization
	void Start () {
		starttransform = transform;
	}
	void OnGUI()
	{
		if(GUI.Button(new Rect (10,10,100,20),"restart")){
			transform.position = starttransform.position;
			transform.rotation = starttransform.rotation;
		}
		}
	// Update is called once per frame
	void Update () {
		//transform.Rotate (transform.localPosition, angle);
		float keyTime=0;
		Vector3 move = Vector3.zero;
		//Mathf.SmoothDamp(cur, Input.GetAxis ("Horizontal")*speed,ref move.z, 1f);
		if (Input.GetAxis ("Horizontal") == 0)
						startinputtime = Time.time;
				else
						 keyTime = startinputtime - Time.time;
		move.z = Input.GetAxis ("Horizontal") * speed * keyTime;

		transform.position += move * Time.deltaTime;
	}
}
