﻿using UnityEngine;
using System.Collections;

public class BotController : MonoBehaviour {

	public DrunkController controller;
	public Transform enemy;

	public float strikeRange = 3f;
	public float retreatRange = 1.5f;
	public float averageMoveForward = -0.1f;
	public float moveNoiseAmplitude = 0.7f;
	public float moveNoiseScale = 0.7f;

	public float currentPower;
	private float currentPerlinX;

	private bool strikeLeft = true;
	void Update () {

		float rangeToEnemy = Mathf.Abs (controller.characterAnimated.transform.position.z - enemy.transform.position.z);

		float randomMove = (Mathf.PerlinNoise (currentPerlinX, 0f) - 0.5f) * 2 * moveNoiseAmplitude + averageMoveForward; 
		currentPerlinX += moveNoiseScale * 0.1f;

		currentPower = randomMove;
		controller.MoveFeet (randomMove);

		if (rangeToEnemy < strikeRange) {
			if (controller.currentStrikeCooldown <=0 )
			{
				if (strikeLeft) controller.StrikeLeft();
				else controller.StrikeRight();
				strikeLeft = !strikeLeft;
			}
		}
	}
}
