using UnityEngine;
using System.Collections;

public class MenuCam : MonoBehaviour {
	public Transform hobo;
	public Vector3 shift;
	// Use this for initialization
	void Start () {
		hobo = transform.parent;
	}
	
	// Update is called once per frame
	void Update () {
		transform.rotation = Quaternion.LookRotation ((hobo.position -transform.position)+shift);
	}

}
