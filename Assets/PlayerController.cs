﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public DrunkController drunk;

	// Update is called once per frame
	void Update () {
		if (drunk != null) {
			float input = Input.GetAxis ("Vertical");
			if (input != 0)
				drunk.MoveBody(input);

			input = Input.GetAxis ("Horizontal");
			if (input != 0)
				drunk.MoveFeet(input);

			if (Input.GetButton("StrikeLeft"))
			{
				drunk.StrikeLeft();
			}
			if (Input.GetButton("StrikeRight"))
			{
				drunk.StrikeRight();
			}
		}
	}
}
