﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Indicator : MonoBehaviour {
	public float tilt;
	public float danger;
	public float max_angle;
	public RectTransform arrow;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		RotateArrow (tilt);
		Tint (danger);
	}
	void RotateArrow(float t)
	{
		Vector3 rot = arrow.localRotation.eulerAngles;
		rot.z = -t * max_angle;
		arrow.localRotation = Quaternion.Euler (rot);
	}
	void Tint(float danger)
	{
		arrow.GetComponent<Image>().color = Color.Lerp(Color.white, new Color(0.7f, 0.02f, 0.02f), danger);

	}
}
