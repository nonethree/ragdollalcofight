﻿using UnityEngine;
using System.Collections;

public class CollideWithWeapon : MonoBehaviour {

	private float cooldown;

	void OnCollisionEnter(Collision coll)
	{
		if (cooldown > 0) return;
		cooldown = 1f;

		Collider targetColl = null;
		if (this.tag.StartsWith("Player") && coll.collider.tag.StartsWith("Enemy")) {
			targetColl = coll.collider;
		}
		else if (this.tag.StartsWith("Enemy") && coll.collider.tag.StartsWith("Player")) {
			targetColl = coll.collider;
		}

		if (targetColl != null) {
			float power = 8f;
			coll.contacts[0].otherCollider.attachedRigidbody.AddForceAtPosition(this.GetComponent<Rigidbody>().velocity * power, coll.contacts[0].point, ForceMode.VelocityChange);
		}
	}

	void Update()
	{
		cooldown -= Time.deltaTime;
	}
}
