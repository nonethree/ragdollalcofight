﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DrunkController : MonoBehaviour {

	public GameObject characterAnimated;
	public GameObject characterRagDoll;
	public Rigidbody forceNode;

	public GameObject nodeToRotate;
	public Indicator indicator;

	public float forceNodeHeight;
	public float forceNodeFallResits;

	public GameController gameController;

	public bool isEnemy;

	public float maxZDeviation = 1f;

	public float pushCooldown = 1f;
	private float currentPushCooldown = 0f;

	public Transform upBody;
	public Transform downBody;
	private bool needPush;
	private float pushDirection;

	public float correctionLevel = 1f;
	public float shiftLevel = 0f;
	public float shiftPositiveCorrectionBounds;
	public float boundsZeroCorrection = 0f;
	public float correctionRestorePower = 0.1f;
	public float correctionDestabilizePower = 0.1f;

	public float strikeCooldown;
	public float currentStrikeCooldown = 0f;
	private float movePower = 0.2f;

	public int podgon = 1;

	public int liveCount = 5;
	public float penaltyCorrectionBounds = 0.02f;
	public float penaltyRestorePower = 0.1f;
	public float penaltyDestabilizePower = 0.2f;

	public float knockOutTime = 2f;

	private float currentKnockOut;
	private List<KeyValuePair<Transform, Transform>> collection = new List<KeyValuePair<Transform, Transform>>();

	private float strikeDelay;
	private Vector3 orig_start;
	private Vector3 orig_end;
	private Vector3 rag_start;
	private Vector3 rag_end;

	private int maxLive;

	void Start()
	{
		this.GetComponent<Customization> ().Customize ();
		GenerateTree ();
		maxLive = liveCount;
	}

	public void GenerateTree()
	{
		StopAllCoroutines ();
		collection.Clear ();

		var components = characterAnimated.GetComponentsInChildren<Transform> ();

		var components2 = characterRagDoll.GetComponentsInChildren<Transform> ();
		IEnumerator itR = components2.GetEnumerator ();

		while (itR.MoveNext()) {

			if (((Transform)itR.Current).GetComponent<Rigidbody>() == null) continue;

			var rb = ((Transform)itR.Current);

			foreach (var t in components)
			{
				if (t.name == rb.name)
				{
					collection.Add(new KeyValuePair<Transform, Transform>(t, rb));
					break;
				}
			}

			Rigidbody rBody = rb.GetComponent<Rigidbody>();
			if (rBody.GetComponent<CollideWithWeapon>() == null)
			{
				rBody.gameObject.AddComponent<CollideWithWeapon>();
			}
			if (rBody.tag.EndsWith("Weapon"))
				rBody.tag = isEnemy ? "EnemyWeapon" : "PlayerWeapon";
			else
				rBody.tag = isEnemy ? "Enemy" : "Player";

			if (rBody != null)
			{
				//if (rBody.drag == 0) 
				rBody.drag = 5f;
				rBody.angularDrag = 5f;
				//rBody.interpolation = RigidbodyInterpolation.Interpolate;
			}


		}

		StartCoroutine ("testC");
	}
	
	public void MoveBody(float move)
	{
		if (currentPushCooldown > 0) return;
		if (move != 0f) {

			//float moveUp = move * 0.5f;
			//downBody.GetComponent<Rigidbody>().AddForce(new Vector3(0f, 0f, move), ForceMode.VelocityChange);
			needPush = true;
			pushDirection = Mathf.Sign(move);
			currentPushCooldown = pushCooldown;
		}
	}

	public void MoveFeet(float move)
	{
		if (move != 0f) {
			move *= movePower * 0.2f * Mathf.Max(0f, 1 - Mathf.Abs(shiftLevel));
			//Debug.Log(move.ToString());
			downBody.GetComponent<Rigidbody>().AddForce(new Vector3(0f, 0f, move), ForceMode.VelocityChange);
			upBody.GetComponent<Rigidbody>().AddForce(new Vector3(0f, 0f, move / 2), ForceMode.VelocityChange);
		}
	}

	public void StrikeLeft()
	{
		if (strikeDelay <= 0) {
			// blabla 
			strikeDelay = strikeCooldown;
			characterAnimated.GetComponent<Animator>().SetTrigger("StrikeLeft");
		}
	}

	public void StrikeRight()
	{
		if (strikeDelay <= 0) {
			// blabla 
			strikeDelay = strikeCooldown;
			characterAnimated.GetComponent<Animator>().SetTrigger("StrikeRight");
		}
	}
	
	void Update()
	{
		if (strikeDelay > 0) {

			strikeDelay -= Time.deltaTime;
			if (strikeDelay < 0)
			{
				//characterAnimated.GetComponent<Animator>().SetTrigger("StrikeLeft");
			}
		}
		
		currentPushCooldown -= Time.deltaTime;

		shiftLevel = (downBody.position.z - upBody.position.z - boundsZeroCorrection) / maxZDeviation;

		float sShift = Mathf.Abs (shiftLevel);

		if (sShift < shiftPositiveCorrectionBounds) {
			float acc = (1 - sShift / shiftPositiveCorrectionBounds) * correctionRestorePower / 60;
			correctionLevel = Mathf.Min (1f, correctionLevel + acc);
		} else {
			float acc = (sShift - shiftPositiveCorrectionBounds) / (1f - shiftPositiveCorrectionBounds) * correctionDestabilizePower / 60;
			correctionLevel = Mathf.Max (0f, correctionLevel - acc);

			float distDragLevel = Mathf.Sign(shiftLevel) * (sShift - shiftPositiveCorrectionBounds) * 0.1f;
			//downBody.GetComponent<Rigidbody>().AddForce(new Vector3(0f, 0f, distDragLevel), ForceMode.VelocityChange);
			//upBody.GetComponent<Rigidbody>().AddForce(new Vector3(0f, 0f, distDragLevel), ForceMode.VelocityChange);
			//upBody.GetComponent<Rigidbody>().AddForce(new Vector3(0f, 0f, distDragLevel), ForceMode.VelocityChange);
		}

		characterAnimated.GetComponent<Animator>().SetFloat("Speed", downBody.GetComponent<Rigidbody>().velocity.z * podgon);
		characterAnimated.GetComponent<Animator>().SetFloat("Shift", shiftLevel * podgon * 4);

		//Vector3 oldPos = characterAnimated.transform.position;
		//characterAnimated.transform.position = new Vector3 (oldPos.x, oldPos.y, (downBody.position.z + upBody.position.z) / 2);
		//characterAnimated.transform.position = Vector3.Lerp(characterAnimated.transform.position, new Vector3 (oldPos.x, oldPos.y, downBody.position.z), 0.1f);
		//characterAnimated.transform.position = new Vector3 (oldPos.x, oldPos.y, downBody.position.z);
		//characterAnimated.transform.rotation = Quaternion.Euler(new Vector3(- 10 * shiftLevel, 0f, 0f));
		//nodeToRotate.transform.rotation = Quaternion.Euler(new Vector3(30 * shiftLevel, 0f, 0f));
		currentKnockOut = correctionLevel == 0f ? currentKnockOut + Time.deltaTime : 0f;
		if (currentKnockOut >= knockOutTime) {
			Restore();
		}

		if (indicator != null) {
			indicator.tilt = Mathf.Clamp(shiftLevel, -1f, 1f);
			indicator.danger = (float)(maxLive - liveCount) / (float)maxLive;
		}
	}

	void Restore()
	{
		if (liveCount == 0) {
			Dead ();
			return;
		}
		liveCount--;
		shiftPositiveCorrectionBounds -= penaltyCorrectionBounds;
		correctionDestabilizePower += penaltyDestabilizePower;
		correctionRestorePower -= penaltyRestorePower;

		currentKnockOut = 0;
		correctionLevel = 1;
		currentStrikeCooldown = 0;
		currentPushCooldown = 0;
		downBody.GetComponent<Rigidbody>().MovePosition(new Vector3(downBody.transform.position.x, 
		     downBody.transform.position.y, upBody.transform.position.z));
		characterAnimated.transform.position = downBody.transform.position;
	}

	void Dead()
	{
		gameController.EndGame ();
	}

	void OnDrawGizmos()
	{
		Debug.DrawLine (orig_start, orig_end, Color.green);
		Debug.DrawLine (rag_start, rag_end, Color.yellow);
	}

	IEnumerator testC()
	{

		while (true) {
			yield return new WaitForFixedUpdate ();

			
			//characterAnimated.transform.rotation = Quaternion.Euler(new Vector3(45 * shiftLevel, 0f, 0f));
			//characterAnimated.transform.rotation = Quaternion.Euler(new Vector3(45 * upBody.GetComponent<Rigidbody>().velocity.z, 0f, 0f));
			
			
			
			Vector3 oldPos = characterAnimated.transform.position;
			//characterAnimated.transform.position = new Vector3 (oldPos.x, oldPos.y, (downBody.position.z + upBody.position.z) / 2);
			characterAnimated.transform.position = new Vector3 (oldPos.x, oldPos.y, downBody.position.z);
			//upBody.GetComponent<Rigidbody>().AddForce(new Vector3(0f, forceNodeHeight - upBody.position.y, 0f) * correctionLevel, ForceMode.VelocityChange);
			//Debug.Log((forceNodeHeight - upBody.position.y).ToString());
			//downBody.GetComponent<Rigidbody>().AddForce(new Vector3(0f, 0f, (upBody.position.z - downBody.position.z) * 0.05f), ForceMode.VelocityChange);

			if (needPush)
			{
				forceNode.AddForce(new Vector3(0f, 0f, 18f * pushDirection), ForceMode.VelocityChange);
				needPush = false;
			}

			foreach (var pair in collection) {
				Transform tr = pair.Key;
				Transform rb = pair.Value;

				Rigidbody rBody = rb.GetComponent<Rigidbody> ();
				if (rBody != null) {

					//Transform rbt = rb.GetComponent<Transform>();	
					float alpha = shiftLevel;
					Vector3 trTransform = tr.position;
					Vector3 withZeroZ = trTransform;
					float yB = withZeroZ.y;
					withZeroZ.y = 0;
					withZeroZ += (new Vector3(0f, Mathf.Cos(alpha), Mathf.Sin(alpha))) * yB;
					Vector3 delta = withZeroZ - rb.position;
					//delta.z *= 0.4f;

					/*
					if (rBody.GetComponent<Freeze>() != null && strikeDelay > 0)
					{
						rb.position = tr.position;
						rb.rotation = rb.rotation;
					}
					else*/
					{

						float animCorrection = correctionLevel * 0.3f;
						if (rBody.GetComponent<Freeze>() != null && strikeDelay > 0)
							animCorrection *= 5f;
						//rBody.AddForceAtPosition(new Vector3(0f, 0f, -1f) * shiftLevel, rb.position, ForceMode.VelocityChange);
						//rBody.AddForce (delta * animCorrection, ForceMode.VelocityChange);
						rBody.AddForceAtPosition(delta * animCorrection, rb.position, ForceMode.VelocityChange);
						//Vector3 axis1, axis2;
						//float angle1, angle2;
						//rb.rotation.ToAngleAxis(out angle1, out axis1);
						//tr.rotation.ToAngleAxis(out angle2, out axis2);
						//if(rBody.name == "Bip001 L Forearm"){
						rag_start = rb.GetComponent<Transform>().position + rb.GetComponent<Transform>().right;
						rag_end = rb.GetComponent<Transform>().position - rb.GetComponent<Transform>().right;
						orig_start = tr.position + tr.right;
						orig_end = tr.position -tr.right;
						rBody.AddTorque(Vector3.Cross((rag_end -rag_start),(orig_end- orig_start)) * (2 * animCorrection), ForceMode.VelocityChange);

						rag_start = rb.GetComponent<Transform>().position + rb.GetComponent<Transform>().up;
						rag_end = rb.GetComponent<Transform>().position - rb.GetComponent<Transform>().up;
						orig_start = tr.position + tr.up;
						orig_end = tr.position -tr.up;
						rBody.AddTorque(Vector3.Cross((rag_end -rag_start),(orig_end- orig_start)) * (2 * animCorrection), ForceMode.VelocityChange);


						rag_start = rb.GetComponent<Transform>().position + rb.GetComponent<Transform>().forward;
						rag_end = rb.GetComponent<Transform>().position - rb.GetComponent<Transform>().forward;
						orig_start = tr.position + tr.forward;
						orig_end = tr.position -tr.forward;
						rBody.AddTorque(Vector3.Cross((rag_end -rag_start),(orig_end- orig_start)) * (2 * animCorrection), ForceMode.VelocityChange);
						}
					//}
					//rBody.angularVelocity = - (tr.rotation.eulerAngles - rb.rotation.eulerAngles) / 2;

					//print(); 
				}

				//rBody.MoveRotation(tr.rotation);
				//rBody.MovePosition(tr.position);
				//rb.position = Vector3.Lerp(rb.position, tr.position, factor);
				//rb.rotation = Quaternion.Lerp(rb.rotation, tr.rotation, 1);

			}

		}
	}

	IEnumerator randomPush()
	{
		while (true)
		{
			//mainPart.AddForce(new Vector3(0f, 30f, 0f), ForceMode.Impulse);
			//anim.enabled = !anim.enabled;
			var randPush = Random.insideUnitSphere * 20;
			forceNode.AddForce(randPush, ForceMode.Impulse);
			yield return new WaitForSeconds(1f);
		}
	}
}



